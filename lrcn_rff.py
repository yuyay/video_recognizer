#! encoding=UTF-8
__author__ = 'Yuya Yoshikawa'

import chainer
import chainer.functions as F
import chainer.links as L
import numpy as np
from chainer import cuda

from alex import Alex
from vgg import VGG16

from sklearn.kernel_approximation import RBFSampler
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score


class AlexLRCN(chainer.Chain):

    n_channels = 3
    insize = 227

    def __init__(self, n_classes=101, fc=7, n_units=256, gamma=1.0, n_random_features=1000):
        super(AlexLRCN, self).__init__(
            cnn=Alex(n_classes=1000),
            fc1=L.Linear(4096, n_units),
            lstm1=L.LSTM(n_units, n_units),
            lstm2=L.LSTM(n_units, n_units),
            fc2=L.Linear(n_units, n_classes)
        )
        self.n_classes = n_classes
        self.fc = fc
        self.train = True
        self.gamma = gamma # gamma for RBFSampler
        self.n_random_features = n_random_features # n_components for RBFSampler
        self.RF = RBFSampler(gamma=gamma, n_components=n_random_features, random_state=0)
        self.RF.fit(np.random.random((1, self.n_classes)))
        self.clf = SGDClassifier()

    def reset_state(self):
        self.lstm1.reset_state()
        self.lstm2.reset_state()


    def forward_one(self, x):
        """

        :param x: (n_frames, n_channels, width, height)
        :return: (n_frames, n_classes)
        """

        n_frames, n_channels, width, height = x.shape
        self.reset_state()

        if self.fc == 6:
            h = self.cnn.get_fc6_output(x, self.train)
        elif self.fc == 7:
            h = self.cnn.get_fc7_output(x, self.train)
        h = F.relu(self.fc1(h))

        for i in range(n_frames):
            h2 = F.relu(self.lstm1(F.reshape(h[i], (1, -1))))
            h2 = F.relu(self.lstm2(h2))
            if i == 0:
                h3 = h2
            if i > 0:
                h3 = F.concat((h3, h2), axis=0)

        h = self.fc2(h3)

        return h


    def __call__(self, x, t):
        """

        :param x: (batchsize, n_frames, n_channels, width, height)
        :param t: (batchsize,)
        :return:
        """

        batchsize, n_frames, n_channels, width, height = x.shape

        mean_loss = 0.
        mean_video_accuracy = 0.
        mean_frame_accuracy = 0.

        for i in range(batchsize):
            h = self.forward_one(x[i])
            mean_loss += F.softmax_cross_entropy(h, t[i])
            # mean_video_accuracy += F.accuracy(F.reshape(F.sum(F.softmax(h), axis=0), (1, -1)), F.reshape(t[i, 0], (1,)), ignore_label=-1)
            mean_frame_accuracy += F.accuracy(h, t[i], ignore_label=-1)

            try:
                z = np.mean(self.RF.transform(cuda.to_cpu(h.data)), axis=0)
            except ValueError:
                print h.shape
                raise ValueError

            if i == 0:
                Z = z[np.newaxis, :]
                # Z = Z.reshape((1, self.n_random_features))
            else:
                Z = np.concatenate((Z, z[np.newaxis, :]), axis=0)

        t = cuda.to_cpu(t.data)
        if self.train:
            self.clf.partial_fit(Z, t[:, 0], classes=range(self.n_classes))
            y = self.clf.predict(Z)
            mean_video_accuracy = accuracy_score(t[:, 0], y)
        else:
            y = self.clf.predict(Z)
            mean_video_accuracy = accuracy_score(t[:, 0], y)

        mean_loss /= batchsize
        # mean_video_accuracy /= batchsize
        mean_frame_accuracy /= batchsize

        chainer.report({
            'loss': mean_loss,
            'video_accuracy': mean_video_accuracy,
            'frame_accuracy': mean_frame_accuracy
        }, self)

        return mean_loss


    def predict(self, x):
        x.volatile = True

        batchsize, n_frames, n_channels, width, height = x.shape
        y = np.zeros((batchsize, self.n_classes), dtype=np.float32)

        for i in range(batchsize):
            h = self.forward_one(x[i])
            h = F.sum(h, axis=0)
            h = F.softmax(h)
            y[i] = h.data

        return y


    def load_cnn(self, fn):
        chainer.serializers.load_npz(fn, self.cnn)


