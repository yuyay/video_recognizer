#! encoding=UTF-8
__author__ = 'Yuya Yoshikawa'

description = """
Some desription
"""

import argparse
import numpy as np
import os

from predict import ActionPredictor
import lrcn
import mydataset

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('val', help='Path to validation image-label list file')
    parser.add_argument('-g', '--gpu', type=int, help="gpu ID", default=-1)
    parser.add_argument('-a', '--arch', type=str, help="model architecture name {AlexLRCN}", default="AlexLRCN")
    parser.add_argument('-d', '--dataset', type=str, help="dataset name {ucf101,hmdb51,stair_actions}",
                        default="ucf101")
    parser.add_argument('--root', '-R', default='.',
                        help='Root directory path of image files')
    parser.add_argument('-m', '--meanfile', type=str, help="mean file used in training")
    parser.add_argument('-t', '--modelfile', type=str, help="trained modelfile")
    # parser.add_argument('-l', '--labelfile', type=str, help="index-label pair file", default=None)
    parser.add_argument('-v', '--verbose', help="verbose", action="store_true", default=False)
    parser.add_argument('-F', '--lim-frames', type=int, help="number of frames extracted from video", default=30)
    parser.add_argument('-T', '--readtimes', type=int, help="the times reading video randomly", default=20)
    parser.add_argument('-o', '--output-csv', type=str, help="output filename for confusion matrix",
                        default="confusion_matrix.csv")
    args = parser.parse_args()

    archs = {
        'AlexLRCN': lrcn.AlexLRCN,
        # 'VGG16LRCN': lrcn.VGG16LRCN,
        # 'AlexLRCNRFF': lrcn_rff.AlexLRCN,
    }

    dataset_class = {
        "ucf101": 101,
        "hmdb51": 51,
        "stair_actions": 100,
    }

    # load model
    predictor = ActionPredictor(arch=args.arch, dataset=args.dataset, gpu=args.gpu, meanfile=args.meanfile,
                                modelfile=args.modelfile, labelfile=None, verbose=args.verbose)

    # load dataset
    # mean = np.load(args.meanfile)
    # val = PreprocessedDataset(args.val, args.root, mean, predictor.model.insize, lim_frames=30, random=True,
    #                           random_frame=True)

    data = []
    with open(args.val) as f:
        for line in f:
            path, label_s = line.strip().split(" ")
            data.append((os.path.join(args.root, path), int(label_s) - 1))

    # compute confusion matrix
    C = np.zeros((dataset_class[args.dataset], dataset_class[args.dataset]), dtype=np.int)
    for path, t in data:
        X = []
        for j in range(args.readtimes):
            x = mydataset.load_video_as_array(path, lim_frames=args.lim_frames,
                                              lim_height=predictor.model.insize,
                                              lim_width=predictor.model.insize, n_channels=3, random_frame=True)
            X.append(x)
        X = np.asarray(X, dtype=np.float32)
        Y, prob = predictor.predict(X, topk=1)
        C[t, Y[0]] += 1
    # C = C / C.sum(axis=1).reshape(-1, 1)

    # save
    np.savetxt(args.output_csv, C, delimiter=",", fmt="%d")
