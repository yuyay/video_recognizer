#! encoding=UTF-8
__author__ = 'Yuya Yoshikawa'

import chainer
import numpy as np
import random
import cv2
import os

def load_video_as_array(path, lim_frames=100, lim_height=100, lim_width=100, n_channels=3, random_frame=True):
    cap = cv2.VideoCapture(path)
    height = int(round(cap.get(cv2.CAP_PROP_FRAME_HEIGHT), 0))
    width = int(round(cap.get(cv2.CAP_PROP_FRAME_WIDTH), 0))
    fps = int(round(cap.get(cv2.CAP_PROP_FPS), 0))
    n_frames = int(round(cap.get(cv2.CAP_PROP_FRAME_COUNT), 0))

    if lim_frames < 0:
        lim_frames = n_frames
    frames = np.zeros((lim_frames, n_channels, lim_height, lim_width))

    if random_frame and lim_frames >= 0 and n_frames - lim_frames + 1 >= 0:
        start = random.randint(0, n_frames - lim_frames + 1)
        end = start + lim_frames
    else:
        start = int(n_frames / 2) - int(lim_frames / 2)
        end = int(n_frames / 2) + int(lim_frames / 2)

    j = 0
    for i in range(0, end):
        ret, frame = cap.read()
        if ret == False:
            break
        if start <= i < end:
            # resize
            frame = cv2.resize(frame, (lim_width, lim_height))

            frame = np.transpose(frame, (2, 0, 1))
            frames[j, :, :, :] = frame
            j += 1

    cap.release()

    return frames


class LabeledVideoDataset(chainer.dataset.DatasetMixin):
    def __init__(self, path, root, crop_size, lim_frames=-1, random=True, random_frame=True):
        self.path = path
        self.root = root
        self.crop_size = crop_size
        self.lim_frames = lim_frames
        self.random = random #
        self.random_frame = random_frame # extract frames randomly from all the frames

        files = []
        labels = []
        with open(path) as f:
            for line in f:
                fn, label = line.strip().split(" ")
                files.append(os.path.join(root, fn))
                labels.append(int(label) - 1) # -1 for ucf101

        self.files = files
        self.labels = np.asarray(labels, dtype=np.int32)

    def __len__(self):
        return len(self.files)

    def get_example(self, i):
        video = load_video_as_array(self.files[i], lim_frames=self.lim_frames, lim_height=self.crop_size,
                                    lim_width=self.crop_size, random_frame=self.random_frame)
        video = video.astype(np.float32)
        label = self.labels[i]
        if self.lim_frames > 0:
            label_seq = label * np.ones((self.lim_frames,), dtype=np.int32)
        else:
            label_seq = label * np.ones((video.shape[0],), dtype=np.int32)

        return video, label_seq


class PreprocessedDataset(chainer.dataset.DatasetMixin):

    def __init__(self, path, root, mean, crop_size, lim_frames=-1, random=True, random_frame=True):
        self.base = LabeledVideoDataset(path, root, crop_size=crop_size, lim_frames=lim_frames,
                                        random=random, random_frame=random_frame)
        self.mean = mean
        self.crop_size = crop_size
        self.random = random
        self.random_frame = random_frame

    def __len__(self):
        return len(self.base)

    def get_example(self, i):
        # It reads the i-th image/label pair and return a preprocessed image.
        # It applies following preprocesses:
        #     - Cropping (random or center rectangular)
        #     - Random flip
        #     - Scaling to [0, 1] value
        crop_size = self.crop_size

        video, label = self.base[i]
        f, c, h, w = video.shape

        if self.random:
            # Randomly crop a region and flip the image
            top = random.randint(0, h - crop_size)
            left = random.randint(0, w - crop_size)
            if random.randint(0, 1):
                video = video[:, :, :, ::-1]
        else:
            # Crop the center
            top = (h - crop_size) // 2
            left = (w - crop_size) // 2
        bottom = top + crop_size
        right = left + crop_size

        video = video[:, :, top:bottom, left:right]
        video -= self.mean[:, top:bottom, left:right]
        video /= 255

        return video, label
