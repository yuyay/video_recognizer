# Human Action Recognition Module for Videos 

## Installation

This module (repository) is managed by Git, and trained model parameter files are managed by git-lfs.
After cloning this repository by git command, move to this repository annd execute the following command to download the trained model parameters.
```
git lfs pull
```

## Execution Environment

We confirmed that this module works on the following environment.

- ubuntu 16.04
- cuda 8.0
- cudnn 5
- ffmpeg 
- python 2.7
    - chainer 1.24.0
    - cupy 1.0.0.1
    - opencv 3.2
    - numpy 1.13.1
    - scipy 0.19.1
    - scikit-learn 0.18.2
    
We provide the docker image that the above environment is installed.
You can download it by the following command (installation of nvidia-docker is necessary).

```
nvidia-docker pull yuyay/vidrec:0.9
```


## Usage

### Dataset preparation (WIP)

### Training (WIP)

See help `python train.py -h`.

### Prediction (WIP)

See help `python predict.py -h`.

#### Example

```
python predict.py data/stair_actions/train/baby_crawling/a102-0003Y.mp4 \
-d stair_actions \
-m meanfiles/alex_stair_actions_mean.npy \
-t pretrained_models/lrcn_stair_actions_trained_model \
-F -1 \
-g 0 \
-l samples/stair_actions/labellist.tsv
```
