#!/usr/bin/env python
import argparse
import sys

import numpy as np
import random

import chainer
import mydataset


def compute_mean(dataset):
    print('compute mean video')
    sum_video = 0
    N = len(dataset)
    for i, (video, _) in enumerate(dataset):
        sum_video += np.mean(video, axis=0)
        sys.stderr.write('{} / {}\r'.format(i, N))
        sys.stderr.flush()
    sys.stderr.write('\n')
    return 1. * sum_video / N


def main():
    parser = argparse.ArgumentParser(description='Compute videos mean array')
    parser.add_argument('dataset',
                        help='Path to training image-label list file')
    parser.add_argument('--root', '-R', default='.',
                        help='Root directory path of image files')
    parser.add_argument('--output', '-o', default='mean.npy',
                        help='path to output mean array')
    parser.add_argument('--crop-size', '-c', default='224', type=int,
                        help='crop size for images. 224 for googlenet')
    args = parser.parse_args()

    dataset = mydataset.LabeledVideoDataset(args.dataset, args.root, crop_size=args.crop_size)
    mean = compute_mean(dataset)
    np.save(args.output, mean)


if __name__ == '__main__':
    main()
