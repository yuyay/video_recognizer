#! encoding=UTF-8
__author__ = 'Yuya Yoshikawa'

description = """
Some desription
"""

import argparse
import random
import os

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('dataset', type=str, help="dataset file", default="videolist.txt")
    parser.add_argument('-o', '--output', type=str, help="output directory", default="./")
    parser.add_argument('-v', '--valsize', type=int, help="size of validation data", default=10000)
    args = parser.parse_args()    

    # load dataset
    with open(args.dataset) as f:
        data = []
        for line in f:
            data.append(line)

    # setting for random
    random.seed(1)
    indices = range(len(data))
    random.shuffle(indices)

    # split dataset into train and val
    valdata = []
    for i in indices[:args.valsize]:
        valdata.append(data[i])

    traindata = []
    for i in indices[args.valsize:]:
        traindata.append(data[i])

    # write
    with open(os.path.join(args.output, "videolist_train.txt"), "w") as f:
        f.writelines(traindata)
    with open(os.path.join(args.output, "videolist_val.txt"), "w") as f:
        f.writelines(valdata)
