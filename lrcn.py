#! encoding=UTF-8
__author__ = 'Yuya Yoshikawa'

import chainer
import chainer.functions as F
import chainer.links as L
import numpy as np

from alex import Alex
# from vgg import VGG16

from mean_average_precision import single_label_average_precision

class AlexLRCN1(chainer.Chain):

    n_channels = 3
    insize = 227

    def __init__(self, n_classes=101, fc=7, n_units=256):
        super(AlexLRCN1, self).__init__(
            cnn=Alex(n_classes=1000),
            fc1=L.Linear(4096, n_units),
            lstm1=L.LSTM(n_units, n_units),
            lstm2=L.LSTM(n_units, n_units),
            fc2=L.Linear(n_units, n_classes)
        )
        self.n_classes = n_classes
        self.fc = fc
        self.train = True

    def reset_state(self):
        self.lstm1.reset_state()
        self.lstm2.reset_state()

    def forward_one(self, x):
        """

        :param x: (n_frames, n_channels, width, height)
        :return: (n_frames, n_classes)
        """

        n_frames, n_channels, width, height = x.shape
        self.reset_state()

        if self.fc == 6:
            h = self.cnn.get_fc6_output(x, self.train)
        elif self.fc == 7:
            h = self.cnn.get_fc7_output(x, self.train)
        h = F.relu(self.fc1(h))

        for i in range(n_frames):
            h2 = F.relu(self.lstm1(F.reshape(h[i], (1, -1))))
            h2 = F.relu(self.lstm2(h2))
            if i == 0:
                h3 = h2
            if i > 0:
                h3 = F.concat((h3, h2), axis=0)

        h = self.fc2(h3)

        return h

    def __call__(self, x, t):
        """

        :param x: (batchsize, n_frames, n_channels, width, height)
        :param t: (batchsize,)
        :return:
        """

        batchsize, n_frames, n_channels, width, height = x.shape

        mean_loss = 0.
        mean_video_accuracy = 0.
        mean_frame_accuracy = 0.
        mean_video_ap = 0.

        for i in range(batchsize):
            h = self.forward_one(x[i])
            mean_loss += F.softmax_cross_entropy(h, t[i])
            mean_video_accuracy += \
                F.accuracy(F.reshape(F.sum(F.softmax(h), axis=0), (1, -1)), F.reshape(t[i, 0], (1,)), ignore_label=-1)
            mean_frame_accuracy += F.accuracy(h, t[i], ignore_label=-1)
            mean_video_ap += single_label_average_precision(F.reshape(t[i, 0], (1,)), F.sum(F.softmax(h), axis=0))

        mean_loss /= batchsize
        mean_video_accuracy /= batchsize
        mean_frame_accuracy /= batchsize
        mean_video_ap /= batchsize

        chainer.report({
            'loss': mean_loss,
            'video_accuracy': mean_video_accuracy,
            'frame_accuracy': mean_frame_accuracy,
            'video_mAP': mean_video_ap
        }, self)

        return mean_loss

    def predict(self, x):
        x.volatile = True

        batchsize, n_frames, n_channels, width, height = x.shape
        y = np.zeros((batchsize, self.n_classes), dtype=np.float32)

        for i in range(batchsize):
            h = self.forward_one(x[i])
            h = F.softmax(h)
            h = F.sum(h, axis=0)
            y[i] = chainer.cuda.to_cpu(h.data)

        return y

    def load_cnn(self, fn):
        chainer.serializers.load_npz(fn, self.cnn)

AlexLRCN = AlexLRCN1
