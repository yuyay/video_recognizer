#!/usr/bin/env python

from __future__ import print_function
import argparse
import random

import numpy as np

import chainer
from chainer import training
from chainer.training import extensions

import lrcn
import lrcn_rff
from mydataset import PreprocessedDataset

if __name__ == '__main__':
    archs = {
        'AlexLRCN': lrcn.AlexLRCN,
        # 'VGG16LRCN': lrcn.VGG16LRCN,
        # 'AlexLRCNRFF': lrcn_rff.AlexLRCN,
    }

    dataset_class = {
        "ucf101": 101,
        "hmdb51": 51,
        "stair_actions": 100,
    }

    parser = argparse.ArgumentParser(description="")
    parser.add_argument('train', help='Path to training image-label list file')
    parser.add_argument('val', help='Path to validation image-label list file')
    parser.add_argument('--arch', '-a', choices=archs.keys(), default='AlexLRCN',
                        help='Network architecture')
    parser.add_argument('--dataset', '-d', choices=dataset_class.keys(), default='ucf101',
                        help='Dataset name')
    parser.add_argument('--batchsize', '-B', type=int, default=32,
                        help='Learning minibatch size')
    parser.add_argument('--epoch', '-E', type=int, default=10,
                        help='Number of epochs to train')
    parser.add_argument('--gpu', '-g', type=int, default=-1,
                        help='GPU ID (negative value indicates CPU')
    parser.add_argument('--initmodel',
                        help='Initialize the model from given file')
    parser.add_argument('--loaderjob', '-j', type=int,
                        help='Number of parallel data loading processes')
    parser.add_argument('--mean', '-m', default='mean.npy',
                        help='Mean file (computed by compute_mean.py)')
    parser.add_argument('--resume', '-r', default='',
                        help='Initialize the trainer from given file')
    parser.add_argument('--out', '-o', default='result',
                        help='Output directory')
    parser.add_argument('--root', '-R', default='.',
                        help='Root directory path of image files')
    parser.add_argument('--val_batchsize', '-b', type=int, default=250,
                        help='Validation minibatch size')
    parser.add_argument('--n-random-features', '-L', type=int, default=1000,
                        help='Number of random features used in AlexLRCNRFF')
    parser.add_argument('--gamma', type=float, default=1.0,
                        help='gamma parameter for RBFSampler used in AlexLRCNRFF')
    parser.add_argument('--test', action='store_true')
    parser.set_defaults(test=False)
    args = parser.parse_args()

    # Initialize the model to train
    if args.arch == 'AlexLRCN':
        model = archs[args.arch](n_classes=dataset_class[args.dataset], fc=6, n_units=256)
    # elif args.arch == 'AlexLRCNRFF':
    #     model = archs[args.arch](n_classes=dataset_class[args.dataset], fc=6, n_units=256,
    #                              gamma=args.gamma, n_random_features=args.n_random_features)
    else:
        raise NotImplementedError("Invalid modelname: {}".format(args.arch))
    if args.initmodel:
        print('Load model from', args.initmodel)
        # chainer.serializers.load_npz(args.initmodel, model)
        model.load_cnn(args.initmodel)

    if args.gpu >= 0:
        chainer.cuda.get_device(args.gpu).use()  # Make the GPU current
        model.to_gpu()

    # Load the datasets and mean file
    mean = np.load(args.mean)
    train = PreprocessedDataset(args.train, args.root, mean, model.insize, lim_frames=30, random=True, random_frame=True)
    val = PreprocessedDataset(args.val, args.root, mean, model.insize, lim_frames=30, random=False, random_frame=False)
    # These iterators load the images with subprocesses running in parallel to
    # the training/validation.
    train_iter = chainer.iterators.MultiprocessIterator(
        train, args.batchsize, n_processes=args.loaderjob)
    val_iter = chainer.iterators.MultiprocessIterator(
        val, args.val_batchsize, repeat=False, n_processes=args.loaderjob)

    # Set up an optimizer
    # optimizer = chainer.optimizers.MomentumSGD(lr=0.01, momentum=0.9)
    optimizer = chainer.optimizers.Adam()
    optimizer.setup(model)

    # Set up a trainer
    updater = training.StandardUpdater(train_iter, optimizer, device=args.gpu) # single GPU
    # updater = training.ParallelUpdater(train_iter, optimizer,
    #                                   devices={'main': args.gpu, 'second': args.gpu+1}
    #                                   ) # multi GPU
    trainer = training.Trainer(updater, (args.epoch, 'epoch'), args.out)

    val_interval = (10 if args.test else 10000), 'iteration'
    log_interval = (10 if args.test else 1000), 'iteration'

    # Copy the chain with shared parameters to flip 'train' flag only in test
    eval_model = model.copy()
    eval_model.train = False

    trainer.extend(extensions.Evaluator(val_iter, eval_model, device=args.gpu),
                   trigger=val_interval)
    trainer.extend(extensions.dump_graph('main/loss'))
    trainer.extend(extensions.snapshot(), trigger=val_interval)
    trainer.extend(extensions.snapshot_object(
        model, 'model_iter_{.updater.iteration}'), trigger=val_interval)
    # Be careful to pass the interval directly to LogReport
    # (it determines when to emit log rather than when to read observations)
    trainer.extend(extensions.LogReport(trigger=log_interval))
    trainer.extend(extensions.observe_lr(), trigger=log_interval)
    trainer.extend(extensions.PrintReport([
        'epoch', 'iteration', 'main/loss', 'validation/main/loss',
        'main/video_accuracy', 'validation/main/video_accuracy',
        'main/video_mAP', 'validation/main/video_mAP', 'lr'
    ]), trigger=log_interval)
    trainer.extend(extensions.ProgressBar(update_interval=10))

    if args.resume:
        chainer.serializers.load_npz(args.resume, trainer)

    trainer.run()
