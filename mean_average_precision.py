#! encoding=UTF-8
__author__ = 'Yuya Yoshikawa'

description = """
Some desription
"""

import argparse
from chainer import cuda
import numpy as np

def single_label_average_precision(y_true, y_scores, topk=-1):
    # xp = cuda.get_array_module(y_scores)
    y_true = cuda.to_cpu(y_true.data)
    y_scores = cuda.to_cpu(y_scores.data)
    index, = np.where(np.argsort(y_scores)[::-1] == y_true)
    return 1.0 / (index[0] + 1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-t', '--test', type=str, help="", default="")
    args = parser.parse_args()    
