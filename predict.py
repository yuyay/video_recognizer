#! encoding=UTF-8
__author__ = 'Yuya Yoshikawa'

description = """
Predict a video label from a given video file
"""

import argparse

import mydataset
import lrcn

import numpy as np
import chainer
from chainer import Variable, cuda
import chainer.functions as F
import cupy
import random


class ActionPredictor(object):
    archs = {
        'AlexLRCN': lrcn.AlexLRCN,
        # 'VGG16LRCN': lrcn.VGG16LRCN,
        # 'AlexLRCNRFF': lrcn_rff.AlexLRCN,
    }
    dataset_class = {
        "ucf101": 101,
        "hmdb51": 51,
        "stair_actions": 100,
    }

    def __init__(self, arch="AlexLRCN", dataset="stair_actions", gpu=-1, meanfile=None, modelfile=None,
                 labelfile=None, verbose=False, random_state=0):
        self.arch = arch
        self.dataset = dataset
        self.gpu = gpu
        self.meanfile = meanfile
        self.modelfile = modelfile
        self.labelfile = labelfile
        self.verbose = verbose
        self.random_state = random_state

        self._set_random_seed(random_state)
        self.model = self.archs[arch](n_classes=self.dataset_class[dataset], fc=6, n_units=256)
        if modelfile:
            chainer.serializers.load_npz(self.modelfile, self.model)
        self.idx_label = self._load_index_label_dict(labelfile)
        if gpu >= 0:
            cuda.get_device_from_id(gpu).use()
            self.model.to_gpu()
        if meanfile:
            self.mean = np.load(meanfile)
        else:
            self.mean = np.array([0.0], dtype=np.float32)

    def _load_index_label_dict(self, labelfile):
        if labelfile:
            idx_label = dict()
            with open(labelfile) as f:
                for line in f:
                    idx_s, label = line.strip().split("\t")
                    idx_label[int(idx_s) - 1] = label
        else:
            idx_label = dict((i, i) for i in range(self.dataset_class[self.dataset]))
        return idx_label

    def _set_random_seed(self, seed=0):
        random.seed(seed)
        np.random.seed(seed)  # set NumPy random seed
        cupy.random.seed(seed)  # set Chainer(CuPy) random seed

    def predict(self, X, topk=10):
        X = np.asarray((X - self.mean[np.newaxis, np.newaxis, :]) / 255., dtype=np.float32)
        X = Variable(cuda.to_gpu(X)) if self.gpu >= 0 else Variable(X)
        Y = mysoftmax(self.model.predict(X))

        y = np.sum(Y, axis=0)
        y_idx = np.argsort(-y)
        result = []
        for k in range(topk):
            result.append((self.idx_label[y_idx[k]], y[y_idx[k]]))
            if self.verbose:
                print("{}\t{}: {}".format(k + 1, self.idx_label[y_idx[k]], y[y_idx[k]]))

        return zip(*result)


def mysoftmax(x):
    c = np.max(x, axis=1).reshape(-1, 1)
    exp_a = np.exp(x - c)
    sum_exp_a = np.sum(exp_a, axis=1).reshape(-1, 1)
    y = exp_a / sum_exp_a
    return y


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("videofile", help="video filename")
    parser.add_argument('-g', '--gpu', type=int, help="gpu ID", default=-1)
    parser.add_argument('-a', '--arch', type=str, help="model architecture name {AlexLRCN}", default="AlexLRCN")
    parser.add_argument('-d', '--dataset', type=str, help="dataset name {ucf101,hmdb51,stair_actions}",
                        default="ucf101")
    parser.add_argument('-m', '--meanfile', type=str, help="mean file used in training")
    parser.add_argument('-t', '--modelfile', type=str, help="trained modelfile")
    parser.add_argument('-l', '--labelfile', type=str, help="index-label pair file", default=None)
    parser.add_argument('-v', '--verbose', help="verbose", action="store_true", default=False)
    parser.add_argument('-F', '--lim-frames', type=int, help="number of frames extracted from video", default=30)
    parser.add_argument('-k', '--topk', type=int, help="the number of actions displayed", default=10)
    parser.add_argument('-T', '--readtimes', type=int, help="the times reading video randomly", default=30)
    args = parser.parse_args()

    # load model
    predictor = ActionPredictor(arch=args.arch, dataset=args.dataset, gpu=args.gpu, meanfile=args.meanfile,
                                modelfile=args.modelfile, labelfile=args.labelfile, verbose=args.verbose)

    # load video
    X = []
    for j in range(args.readtimes):
        x = mydataset.load_video_as_array(args.videofile, lim_frames=args.lim_frames, lim_height=predictor.model.insize,
                                          lim_width=predictor.model.insize, n_channels=3, random_frame=True)
        X.append(x)
    X = np.asarray(X, dtype=np.float32)

    # predict actions
    actions, prob = predictor.predict(X, topk=args.topk)
